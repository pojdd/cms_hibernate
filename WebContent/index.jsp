<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>发布文章</title>
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/bootstrap-3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/layout.css">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/index.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/static2/css/zxf_page.css">

  <script src="<%=request.getContextPath()%>/static2/js/jquery-3.4.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/static2/js/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  <script src="<%=request.getContextPath()%>/static2/js/echarts-2.2.7/echarts-all.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/static2/js/zxf_page.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/static2/js/main.js"></script>
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/reset.css">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/style.css">
  <script type="text/javascript">
	/*点击a标签去除默认事件  */
	$(function(){
		$(".showLeft a").on("click",function(event){
			var hrefStr = this.getAttribute("href");
			console.log(hrefStr);
			event.preventDefault();
			$('.content').load(hrefStr);
		});
	$("#recommend").trigger("click");
	});
	</script>
</head>

<body>
  <!-- 外部容器 -->
  <div class="container-layout">
    <div class="top_bar"></div>
    <div class="wrap">
      <!-- 左侧导航 -->
      <div class="nav_left">
        <!-- 标题 -->
        <div class="title">
          <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>看点资讯
        </div>

        <div class="showLeft">
          <ul class="cd-accordion-menu animated">
            <li class="has-children">
              <input type="checkbox" name="group-1" id="group-1" checked>
              <label for="group-1">首页</label>

              <ul>
                <li><a href="./pages/getHomePages">首页展示</a></li>
              </ul>
            </li>

            <li class="has-children">
              <input type="checkbox" name="group-4" id="group-4">
              <label for="group-4">资讯栏目</label>
              <ul>
                <li class="has-children">
                  <input type="checkbox" name="sub-group-3" id="sub-group-3">
                  <label for="sub-group-3">热点资讯</label>

                  <ul>
                    <li><a href="./pages/show.jsp">今日新闻</a></li>
                    <li><a href="#0">趣味视频</a></li>
                    <li><a href="#0">国际</a></li>
                  </ul>
                </li>
                <li>
                  <input type="checkbox" name="sub-group-4" id="sub-group-4">
                  <label for="sub-group-4">体育</label>

                  <ul>
                    <li><a href="./pages/show.jsp">CBA</a></li>
                    <li><a href="#0">英超</a></li>
                  </ul>
                </li>
                <li>
                  <input type="checkbox" name="sub-group-5" id="sub-group-5">
                  <label for="sub-group-5">娱乐</label>

                  <ul>
                    <li><a href="./pages/show.jsp">电视剧</a></li>
                    <li><a href="#0">电影</a></li>
                    <li><a href="#0">音乐</a></li>
                  </ul>
                </li>
              </ul>
            </li>

            <li class="has-children">
              <input type="checkbox" name="group-3" id="group-3">
              <label for="group-3">个人管理</label>

              <ul>
                <li><a href="<%=request.getContextPath()%>/publish">我的发布</a></li>
                <li><a href="./pages/history.jsp">浏览记录</a></li>
                <li><a href="./pages/click.jsp">我的点赞</a></li>
                <li><a href="./pages/collection.jsp">我的收藏</a></li>
                <li><a href="./pages/report.jsp">我的举报</a></li>
                <li><a href="./pages/person.jsp">个人信息</a></li>
              </ul>
            </li>


          </ul>

        </div>
      </div>
      <!-- 右侧内容 -->
      <div class="content_right">
        <div class="top_bar">
          <!-- 用户信息 -->
          <s:if test='%{#session.username!=null}'>
          		<div class="user_info">
	            <img src="<%=request.getContextPath()%>/static2/images/tx.jpg" alt="">
	            <span>${username }</span>
          </div>
          </s:if>
          <div class="login">
          		<s:if test='%{#session.username==null}'>
          			<a href="login.jsp">登录</a>
          		</s:if>
          		<s:else>
          			<a href="exitAction">退出</a>
          		</s:else>
          </div>
        </div>
        <div id="content" class="content">
        	<script type="text/javascript">
        	$('.content').load("./pages/getHomePages");
        	</script>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript" src="<%=request.getContextPath()%>/static2/js/basic.js"></script>

</body>

</html>