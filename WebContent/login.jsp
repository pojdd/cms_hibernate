<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>登录</title>
	<link rel="icon" href="kodinger.jpg" type="image/x-icon" />
	<link rel="stylesheet" href="auth.css">
	<script src="<%=request.getContextPath()%>/static2/js/jquery-3.4.1.min.js"></script>
</head>

<body>
	<div class="lowin ">
		<div class="lowin-brand">
			<img src="kodinger.jpg" alt="logo">
		</div>
		<div class="lowin-wrapper">
			<div class="lowin-box lowin-login">
				<div class="lowin-box-inner">
					<form method="POST">
						<p>震惊！"UC震惊部"出大事儿了！登录查看</p>
						<p>${error }</p>
						<div class="lowin-group int">
							<label for="email">电子邮件 <a href="#" class="login-back-link"
									onclick="document.getElementById('password').style.display='inline'">登录?</a></label>
							<input type="email" autocomplete="email" id="email" name="email"
								class="lowin-input required" style="z-index: 0">
						</div>
						<div class="lowin-group password-group int">
							<label>密码 <a href="#" class="forgot-link"
									onclick="document.getElementById('password').style.display='none'">忘记密码?</a></label>
							<input id="password" type="password" name="password" autocomplete="current-password"
								class="lowin-input" style="z-index: 1">
						</div>
						<button id="send" class="lowin-btn login-btn">
							登录
						</button>

						<div class="text-foot">
							没有账号嘛? <a href="" class="register-link" onclick="document.title='注册'">注册</a>
						</div>
					</form>
				</div>
			</div>

			<div class="lowin-box lowin-register">
				<div class="lowin-box-inner">
					<form action="register" method="POST">
						<p>开始创建账户</p>
						<div class="lowin-group">
							<label>名称</label>
							<input type="text" name="name" autocomplete="name" class="lowin-input">
						</div>
						<div class="lowin-group">
							<label>电子邮件</label>
							<input type="email" autocomplete="email" name="email" class="lowin-input required">
						</div>
						<div class="lowin-group">
							<label>密码</label>
							<input type="password" name="password" autocomplete="current-password" class="lowin-input">
						</div>
						<!-- <button class="lowin-btn">
							注册
						</button> -->
						<input type="submit" class="lowin-btn" value="注册">
						<div class="text-foot">
							已经有账号了? <a href="" class="login-link" onclick="document.title='登录'">登录</a>
						</div>
					</form>
				</div>
			</div>
		</div>

		<footer class="lowin-footer" style="align:center">
			欢迎登陆
		</footer>
	</div>



	<script>
		//为表单的必填文本框添加提示信息（选择form中的所有后代input元素）
		$("form :input.required").each(function () {
			//通过jquery api：$("HTML字符串") 创建jquery对象
			//var $required = $("<p class='high'>*</p>");
			//添加到this对象的父级对象下
			//$(this).parent().append($required);
		});

		//为表单元素添加失去焦点事件
		$("form :input").blur(function () {
			var $parent = $(this).parent();
			$parent.find(".msg").remove(); //删除以前的提醒元素（find()：查找匹配元素集中元素的所有匹配元素）
			//验证邮箱
			if ($(this).is("#email")) {
				var emailVal = $.trim(this.value);
				var regEmail = /.+@.+\.[a-zA-Z]{2,4}$/;
				if (emailVal == "" || (emailVal != "" && !regEmail.test(emailVal))) {
					var errorMsg = " 请输入正确的E-Mail地址！";
					$parent.append("<span class='msg onError'><nobr><lable class='text-foot'>" + errorMsg + "<lable/><nobr/></span>");

				}
				else {
					var okMsg = " 输入正确";
					$parent.find(".high").remove();
					$parent.append("<span class='msg onError'><lable class='text-foot'>" + okMsg + "<lable/></span>");
				}
			}
		}).keyup(function () {
			//triggerHandler 防止事件执行完后，浏览器自动为标签获得焦点
			$(this).triggerHandler("blur");
		}).focus(function () {
			$(this).triggerHandler("blur");
		});

        //点击重置按钮时，通过trigger()来触发文本框的失去焦点事件
	</script>
	<script src="auth.js"></script>
	<script>
		Auth.init({
			login_url: 'login',
			forgot_url: 'forgot'
		});
	</script>

</body>

</html>