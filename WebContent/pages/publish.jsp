<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>看点资讯精选</title>
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/bootstrap-3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/layout.css">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/index.css">
  <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/static2/css/publish.css">
  <script src="<%=request.getContextPath()%>/static2/js/jquery-3.4.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/static2/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  <script src="<%=request.getContextPath()%>/static2/echarts-2.2.7/echarts-all.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/static2/js/main.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/static2/js/publish.js"></script>
  <script type="text/javascript" src="<%=request.getContextPath()%>/static2/js/wangEditor.js"></script>
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/reset.css">
  <link rel="stylesheet" href="<%=request.getContextPath()%>/static2/css/style.css">
</head>

<body>
        <!-- 内容 -->
        <div class="content">
          <div class="search_bar">

            <span class="search_title">我的发布</span>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger  btn-sm" data-toggle="modal"
              data-target=".bs-example-modal-s2">批量删除</button>&nbsp;&nbsp;

            <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target=".bs-example-modal-lg">
              <span class="glyphicon glyphicon-file" aria-hidden="true"></span> 发布文章
            </button>&nbsp;&nbsp;

            <!--  发布文章model -->
            <button type="button" class="btn btn-success btn-md" data-toggle="modal"
              data-target=".bs-example-modal-video">
              <span class="glyphicon glyphicon-film" aria-hidden="true"></span> 发布视频
            </button>

          </div>
          <!-- 搜索end -->
          <div class="news_list">
            <table class="tbl">
              <thead>
                <tr>
                  <th width="50px">编号</th>
                  <th width="200px">文章题目</th>
                  <th width="200px">所属栏目</th>
                  <th>发布时间</th>
                  <th>阅读量</th>
                  <th>点赞量</th>
                  <th>收藏量</th>
                  <th>状态</th>
                  <th width="160px">操作</th>
                </tr>
              </thead>
              <tbody>
              <s:iterator value="list">
                <tr>
                  <td>
                    <input type="checkbox">
                  </td>
                  <td><s:property value="title" /></td>
                  <td>一级</td>
                  <td><s:property value="releaseDate" /></td>
                  <td><s:property value="clickTimes" /></td>
                  <td><s:property value="user_likes.size()"/></td>
                  <td><s:property value="user_collection.size()"/></td>
                  <td>3</td>
                  <td>
                    <span class="readed tag_read">已阅读</span>
                  </td>
                  <td class="optior">
                    <span class="glyphicon glyphicon-trash del_icon" aria-hidden="true" data-toggle="modal"
                      data-target=".bs-example-modal-del"></span>&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-edit edit_icon" aria-hidden="true" data-toggle="modal"
                      data-target=".bs-example-modal-edit"></span>
                  </td>
                </tr>
                </s:iterator>
              </tbody>
            </table>
          </div>
        </div>
        <!-- 内容 -->

      </div>
    </div>
  </div>
  <!-- 编辑 -->
  <div class="modal fade bs-example-modal-edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridSystemModalLabel">编辑文章信息</h4>
        </div>
        <div class="modal-body">

          <form class="form-horizontal">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-1 control-label">标题</label>
              <div class="col-sm-11">
                <input type="email" class="form-control title_input" id="inputEmail3" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-1 control-label">栏目</label>
              <div class="col-sm-11">
                <select class="form-control select_label" name="categoryId">
                  <!--遍历生成栏目列表  -->
                  <option selected="selected" value="0">其他111</option>
                  <optgroup label="一级分类1">

                    <option value="5">二级2</option>

                    <option value="6">二级3</option>

                    <option value="4">二级1</option>

                  </optgroup>

                  <optgroup label="一级分类2">

                    <option value="8">二级分类5</option>

                    <option value="7">二级分类4</option>

                    <option value="9">二级分类6</option>

                  </optgroup>

                  <optgroup label="一级分类3">

                    <option value="10">二级分类7</option>

                    <option value="12">二级分类9</option>

                    <option value="11">二级分类8</option>

                  </optgroup>

                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-1 control-label">内容</label>
              <div class="col-sm-11">
                <textarea class="form-control title_input" rows="3" cols="10"></textarea>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-primary">确定</button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </div>
  <!-- 批量删除model -->
  <div class="modal fade bs-example-modal-s2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-body">确定删除吗？</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-primary">确定</button>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  <!-- 删除model -->
  <div class="modal fade bs-example-modal-del" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-body">确定删除吗？</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-primary">确定</button>
        </div>
      </div>

    </div>
  </div>
  <!-- 发布视频 -->
  <div class="modal fade bs-example-modal-video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridSystemModalLabel">发布视频</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-1 control-label">标题</label>
              <div class="col-sm-11">
                <input type="email" class="form-control title_input" id="inputEmail3" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-1 control-label">栏目</label>
              <div class="col-sm-11">
                <select class="form-control select_label" name="categoryId">
                  <!--遍历生成栏目列表  -->
                  <option selected="selected" value="0">其他111</option>
                  <optgroup label="一级分类1">

                    <option value="5">二级2</option>

                    <option value="6">二级3</option>

                    <option value="4">二级1</option>

                  </optgroup>

                  <optgroup label="一级分类2">

                    <option value="8">二级分类5</option>

                    <option value="7">二级分类4</option>

                    <option value="9">二级分类6</option>

                  </optgroup>

                  <optgroup label="一级分类3">

                    <option value="10">二级分类7</option>

                    <option value="12">二级分类9</option>

                    <option value="11">二级分类8</option>

                  </optgroup>

                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-1 control-label">内容</label>
              <div class="col-sm-11">
                <textarea class="form-control title_input" rows="3" cols="10"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-1 control-label">视频</label>
              <div class="col-sm-11">
                <div class="file-container"
                  style="display:inline-block;position:relative;overflow: hidden;vertical-align:middle">
                  <button class="btn btn-success fileinput-button" type="button">上传视频</button>
                  <input type="file" id="jobData" onchange="publishVideo(this.files[0])"
                    style="position:absolute;top:0;left:0;font-size:34px; opacity:0" accept=".mp4">
                </div>
                <span id="publishVideo" style="vertical-align: middle">未上传文件</span>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-primary">发布</button>
        </div>
      </div>
    </div>
  </div>
  <!--  发布文章model -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridSystemModalLabel">发布文章</h4>
        </div>
        <s:form action="uploads" method="post" enctype="multipart/form-data">
            <s:textfield name="title"></s:textfield>
              <label>栏目</label>
                <select class="form-control select_label" name="categoryId">
                  <!--遍历生成栏目列表  -->
                  <option selected="selected" value="0">其他111</option>
                  <optgroup label="一级分类1">
                    <option value="5">二级2</option>
                    <option value="6">二级3</option>
                    <option value="4">二级1</option>
                  </optgroup>
                  <optgroup label="一级分类2">

                    <option value="8">二级分类5</option>

                    <option value="7">二级分类4</option>

                    <option value="9">二级分类6</option>

                  </optgroup>

                  <optgroup label="一级分类3">

                    <option value="10">二级分类7</option>

                    <option value="12">二级分类9</option>

                    <option value="11">二级分类8</option>
                  </optgroup>
                </select>
            <s:textarea name="content"></s:textarea>
            <s:file name="uploadMovie" label="选择视频"/>
            <s:file name="uploadMusic" label="选择音乐"/>
            <s:file name="uploadImage" label="选择背景"/>
        	<s:submit type="button" class="btn btn-primary">发布</s:submit>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        </s:form>
      </div>
    </div>
  </div>
</body>

</html>