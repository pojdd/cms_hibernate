<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>${title }</title>
  <link rel="stylesheet" href="../static2/bootstrap-3.3.7/css/bootstrap.min.css">

  <script src="../static2/js/jquery-3.4.1.min.js"></script>
  <script src="../static2/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  <script src="../static2/echarts-2.2.7/echarts-all.js"></script>
  <script type="text/javascript" src="../static2/js/wangEditor.js"></script>
  <script type="text/javascript" src="../static2/js/main.js"></script>
  <script type="text/javascript" src="../static2/js/detail.js"></script>
  <link rel="stylesheet" type="text/css" href="../static2/css/detail.css">
  <link rel="stylesheet" href="../static2/css/forum.css">
  <link rel="stylesheet" href="../static2/css/common.css">
</head>

<body>
  	<s:if test='%{#articleid==null}'>
	    <div class="user_info">
		    <script type="text/javascript">
         		 window.location.href = "../404.html";
        	</script>
	    </div>
	</s:if>
  <!-- 外部容器 -->
  <div class="container-layout">
      <!-- 右侧内容 -->

        <!-- 内容 -->
        <div class="content_box">

          <!-- 搜索end -->
          <h4 class="title_big">${title }</h4>

          <div class="new_header">
                <div class="new_header">
                   <div>作者：<b> ${nickname }</b></div>&nbsp;&nbsp;&nbsp;&nbsp;
                  <div>发表时间：<b>${date } </b></div>
                </div>

               <div class="new_header">
               		<p> ${msg } </p>
                  <div><a href="./clooAction?articleid=${articleid }"><span class="glyphicon glyphicon-star"></span>收藏</a><b>${cloo }</b></div>&nbsp;&nbsp;&nbsp;&nbsp;
                  <div><a href="./likeAction?articleid=${articleid }"><span class="glyphicon glyphicon-thumbs-up"></span>点赞</a><b>${likes }</b></div>&nbsp;&nbsp;&nbsp;&nbsp;
                  <div data-toggle="modal" data-target=".bs-example-modal-report"><span class="glyphicon glyphicon-phone-alt"></span>举报</div>
              </div>
          </div>
          <div class="container_img_slide">
               <!--  图片轮播 -->
              <div id="carousel-example-generic" class="carousel slide  slide_img" data-ride="carousel">

              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              </ol>


              <div class="carousel-inner" role="listbox">
                <div class="item active  img_center">
                  <img src="../static2/images/2.jpg" alt="...">

                </div>
                <div class="item img_center">
                  <img src="../static2/images/2.jpg" alt="...">

                </div>
                  ...
                </div>


                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
               <!--  右侧内容 -->
          </div>
           <div class="new_content">
                      <span class="creater_from">原创</span>
                      <%
                      	String content=(String)request.getAttribute("content");
                      	out.print(content);                      
                      %>
          </div>

        </div>


    </div>
  </div>
<div class="modal fade bs-example-modal-report" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">不良信息举报</h4>
      </div>
      <div class="modal-body report_model_body">
          <div class="report_reason">
          <div class="report_title_left">举报原因</div>
            <div class="report_content_right">
                <label>色情&nbsp;<input type="radio" name=""></label>&nbsp;&nbsp;
               <label>违法&nbsp;<input type="radio" name=""></label>&nbsp;&nbsp;
                <label>犯罪&nbsp;<input type="radio" name=""></label>&nbsp;&nbsp;
                 <label>色情&nbsp;<input type="radio" name=""></label>&nbsp;&nbsp;
               <label>违法&nbsp;<input type="radio" name=""></label>&nbsp;&nbsp;
                <label>犯罪&nbsp;<input type="radio" name=""></label>&nbsp;&nbsp;
            </div>
          </div>
        <div class="report_reason_add">
        <div class="report_title_left">原因补充</div>
            <div class="report_content_right">
            <textarea class="text_reason_add"></textarea>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary">举报</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
  </div>

<script type="text/javascript" src="../static2/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="../static2/js/jquery.flexText.js"></script>
<!--textarea高度自适应-->
<script type="text/javascript">
  $(function () {
    $('.content').flexText();
  });
</script>
<!--textarea限制字数-->
<script type="text/javascript">
  function keyUP(t) {
    var len = $(t).val().length;
    if (len > 139) {
      $(t).val($(t).val().substring(0, 140));
    }
  }
</script>
</body>

</html>