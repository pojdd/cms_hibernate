<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Insert title here</title>
</head>

<body>
  <!-- 内容 -->
  <div class="content">
    <div class="search_bar">
      <span class="search_title">首页展示</span>
    </div>
    <!-- 搜索end -->
    <div class="news_list">
      <ul class="list-group">
        <s:iterator value="list">
          <li class="list-group-item">
            <div class="media">
              <div class="media-left">
                <a href="#" class="media-object">
                  <!--  <img  src="./static2/images/tx.jpg" alt="..."> -->
                  <img src="<s:property value="pictureUrls" />" alt="...">
                </a>
              </div>
              <div class="media-body">
                <a class="media-heading title_news" href="<%=request.getContextPath()%>/pages/getDetail?id=<s:property value="id" />" target="view_window"><s:property value="title" /></a>
                <div class="media_content">
                    <s:property value="content" />
                </div>
                <div class="media_option">
                  <div>阅读量:<b><s:property value="clickTimes" /></b></div>
                  <div>点赞量:<b><s:property value="user_likes.size" /></b></div>
                  <div>收藏量:<b><s:property value="user_collection.size" /></b></div>
                </div>
              </div>
            </div>
          </li>
        </s:iterator>
      </ul>
      <!--  页角 -->
      <div class="footer">
      </div>
    </div>
    <!--  页脚 -->

  </div>
  <!-- 内容 -->
</body>

</html>