<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>浏览记录</title>
  <link rel="stylesheet" href="../static2/bootstrap-3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="../static2/css/layout.css">
  <link rel="stylesheet" href="../static2/css/index.css">
  <link rel="stylesheet" type="text/css" href="../static2/css/publish.css">
  <script src="../static2/js/jquery-3.4.1.min.js"></script>
  <script src="../static2/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  <script src="../static2/echarts-2.2.7/echarts-all.js"></script>
  <script type="text/javascript" src="../static2/js/publish.js"></script>
  <script type="text/javascript" src="../static2/js/wangEditor.js"></script>
  <script type="text/javascript" src="../static2/js/main.js"></script>
  <link rel="stylesheet" href="../static2/css/reset.css">
  <link rel="stylesheet" href="../static2/css/style.css">
</head>

<body>
  <!-- 外部容器 -->
  <div class="container-layout">
    <div class="top_bar"></div>
    <div class="wrap">
      <!-- 左侧导航 -->
      <div class="nav_left">
        <!-- 标题 -->
        <div class="title">
          <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>看点资讯
        </div>
        <!-- 列表 -->
        <!--  <ul class="nav_list">
          <li class="current">
            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
            <a href="./index.jsp">首页</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
          </li>
          <li>
            <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
            <a href="./category.jsp">栏目管理</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
          </li>
          <li>
            <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
            <a href="./message.jsp">资讯管理</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
          </li>
          <li>
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            <a href="./user.jsp">用户管理</a>
            <span class="glyphicon glyphicon-chevron-right"></span>
          </li>
        </ul> -->
        <div class="showLeft">
          <ul class="cd-accordion-menu animated">
            <li class="has-children">
              <input type="checkbox" name="group-1" id="group-1" checked>
              <label for="group-1">首页</label>

              <ul>
                <li><a href="../index.jsp">首页展示</a></li>
              </ul>
            </li>

            <li class="has-children">
              <input type="checkbox" name="group-4" id="group-4">
              <label for="group-4">资讯栏目</label>

              <ul>
                <li class="has-children">
                  <input type="checkbox" name="sub-group-3" id="sub-group-3">
                  <label for="sub-group-3">栏目一级</label>

                  <ul>
                    <li><a href="./show.jsp">栏目二级</a></li>
                    <li><a href="#0">栏目二级</a></li>
                  </ul>
                </li>
                <li><a href="#0">新栏目一级</a></li>
                <li><a href="#0">旧栏目二级</a></li>
              </ul>
            </li>

            <li class="has-children">
              <input type="checkbox" name="group-3" id="group-3" checked>
              <label for="group-3">个人管理</label>

              <ul>
                <li><a href="./publish.jsp">我的发布</a></li>
                <li><a href="./history.jsp">浏览记录</a></li>
                <li><a href="./click.jsp">我的点赞</a></li>
                <li><a href="./collection.jsp">我的收藏</a></li>
                <li><a href="./report.jsp">我的举报</a></li>
                <li><a href="./person.jsp">个人信息</a></li>
              </ul>
            </li>


          </ul>
        </div>
      </div>
      <!-- 右侧内容 -->
      <div class="content_right">
        <div class="top_bar">
          <!-- 用户信息 -->
          <div class="user_info">
            <img src="../static2/images/tx.jpg" alt="">
            <span>胡六</span>
          </div>
          <div class="login">
            <a href="./login.jsp">退出</a>
          </div>
        </div>
        <!-- 内容 -->
        <div class="content">
          <div class="search_bar">
            <span class="search_title">浏览记录</span>&nbsp;&nbsp;
          </div>
          <!-- 搜索end -->
          <div class="news_list">
            <table class="tbl">
              <thead>
                <tr>

                  <th width="200px">问章题目</th>
                  <th width="200px">所属栏目</th>
                  <th>发布时间</th>
                  <th>背景音乐</th>
                  <th>阅读量</th>
                  <th>点赞量</th>
                  <th>收藏量</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>发布测试</td>
                  <td>一级</td>
                  <td>2018/09/08</td>
                  <td>3</td>
                  <td>4</td>
                  <td>6</td>
                  <td>6</td>
                </tr>
                <tr>
                  <td>发布测试</td>
                  <td>一级</td>
                  <td>2018/09/08</td>
                  <td>3</td>
                  <td>4</td>
                  <td>6</td>
                  <td>6</td>
                </tr>
                <tr>
                  <td>发布测试</td>
                  <td>一级</td>
                  <td>2018/09/08</td>
                  <td>3</td>
                  <td>4</td>
                  <td>6</td>
                  <td>6</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- 内容 -->

      </div>
    </div>
  </div>

  <script type="text/javascript" src="../static2/js/basic.js"></script>
</body>

</html>