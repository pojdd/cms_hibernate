<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>点赞记录列表</title>
  <link rel="stylesheet" href="../static2/bootstrap-3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="../static2/css/layout.css">
  <link rel="stylesheet" href="../static2/css/index.css">
  <link rel="stylesheet" type="text/css" href="../static2/css/publish.css">
  <script src="../static2/js/jquery-3.4.1.min.js"></script>
  <script src="../static2/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  <script src="../static2/echarts-2.2.7/echarts-all.js"></script>
  <script type="text/javascript" src="../static2/js/publish.js"></script>
  <script type="text/javascript" src="../static2/js/wangEditor.js"></script>
  <script type="text/javascript" src="../static2/js/main.js"></script>
  <link rel="stylesheet" href="../static2/css/reset.css">
  <link rel="stylesheet" href="../static2/css/style.css">

</head>

<body>
  <!-- 内容 -->
  <div class="content">
    <div class="search_bar">
      <span class="search_title">我的点赞</span>&nbsp;&nbsp;
    </div>
    <!-- 搜索end -->
    <div class="news_list">
      <table class="tbl">
        <thead>
          <tr>

            <th width="200px">问章题目</th>
            <th width="200px">所属栏目</th>
            <th>发布时间</th>
            <th>背景音乐</th>
            <th>阅读量</th>
            <th>点赞量</th>
            <th>收藏量</th>
            <th>状态</th>
            <th width="160px">操作</th>
          </tr>
        </thead>
        <tbody>
          <tr>

            <td>发布测试</td>
            <td>一级</td>
            <td>2018/09/08</td>
            <td>3</td>
            <td>4</td>
            <td>6</td>
            <td>已阅读</td>
            <td>已阅读</td>
            <td class="optior">
              <span class="glyphicon glyphicon-thumbs-up" style="color: red" aria-hidden="true" data-toggle="modal"
                data-target=".bs-example-modal-del"></span>&nbsp;&nbsp;
            </td>
          </tr>

        </tbody>
      </table>
    </div>
    <div class="footer">
    </div>
  </div>
  <!-- 内容 -->
  <script type="text/javascript" src="../static2/js/basic.js"></script>
</body>

</html>