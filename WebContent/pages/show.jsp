<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>发布文章</title>
  <link rel="stylesheet" href="../static2/bootstrap-3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="../static2/css/layout.css">
  <link rel="stylesheet" href="../static2/css/index.css">
  <link rel="stylesheet" type="text/css" href="../static2/css/zxf_page.css">

  <script src="../static2/js/jquery-3.4.1.min.js"></script>
  <script src="../static2/js/bootstrap-3.3.7/js/bootstrap.min.js"></script>
  <script src="../static2/js/echarts-2.2.7/echarts-all.js"></script>
  <script type="text/javascript" src="../static2/js/zxf_page.js"></script>
  <script type="text/javascript" src="../static2/js/main.js"></script>
  <link rel="stylesheet" href="../static2/css/reset.css">
  <link rel="stylesheet" href="../static2/css/style.css">
</head>

<body>
  <!-- 外部容器 -->
  <div class="container-layout">
    <div class="top_bar"></div>
    <div class="wrap">
      <!-- 左侧导航 -->
      <div class="nav_left">
        <!-- 标题 -->
        <div class="title">
          <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>看点资讯
        </div>

        <div class="showLeft">
          <ul class="cd-accordion-menu animated">
            <li class="has-children">
              <input type="checkbox" name="group-1" id="group-1">
              <label for="group-1">首页</label>

              <ul>
                <li><a href="#0">首页展示</a></li>
              </ul>
            </li>

            <li class="has-children">
              <input type="checkbox" name="group-4" id="group-4" checked>
              <label for="group-4">资讯栏目</label>

              <ul>
                <li class="has-children">
                  <input type="checkbox" name="sub-group-3" id="sub-group-3" checked>
                  <label for="sub-group-3">栏目一级</label>

                  <ul>
                    <li><a href="../pages/show.jsp">栏目二级</a></li>
                    <li><a href="#0">栏目二级</a></li>
                  </ul>
                </li>
                <li><a href="#0">新栏目一级</a></li>
                <li>
                  <input type="checkbox" name="sub-group-4" id="sub-group-4">
                  <label for="sub-group-4">栏目222级</label>

                  <ul>
                    <li><a href="../pages/show.jsp">栏目222级</a></li>
                    <li><a href="#0">栏目222级</a></li>
                  </ul>
                </li>
              </ul>
            </li>

            <li class="has-children">
              <input type="checkbox" name="group-3" id="group-3">
              <label for="group-3">个人管理</label>

              <ul>
                <li><a href="../pages/publish.jsp">我的发布</a></li>
                <li><a href="../pages/history.jsp">浏览记录</a></li>
                <li><a href="../pages/click.jsp">我的点赞</a></li>
                <li><a href="../pages/collection.jsp">我的收藏</a></li>
                <li><a href="../pages/report.jsp">我的举报</a></li>
                <li><a href="../pages/person.jsp">个人信息</a></li>
              </ul>
            </li>


          </ul>

        </div>
      </div>
      <!-- 右侧内容 -->
      <div class="content_right">
        <div class="top_bar">
          <!-- 用户信息 -->
          <div class="user_info">
            <img src="../static2/images/tx.jpg" alt="">
            <span>胡六</span>
          </div>
          <div class="login">
            <a href="../login.jsp">退出</a>
          </div>
        </div>
        <!-- 内容 -->
        <div class="content">
          <div class="search_bar">
            <span class="search_title">首页展示</span>
          </div>
          <!-- 搜索end -->
          <div class="news_list">
            <ul class="list-group">
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <a href="#" class="media-object">
                      <!--  <img  src="../static2/images/tx.jpg" alt="..."> -->
                      <img src="../static2/images/2.jpg" alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <a class="media-heading title_news" href="../pages/detail.jsp" target="view_window">热点新闻</a>
                    <div class="media_content">
                      这里是新闻内这里是新这里是新闻内容你发附件闻内容你发附新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里件容这里是新闻内容你发附件你发附件
                    </div>
                    <div class="media_option">
                      <div>阅读量:<b>21</b></div>
                      <div>点赞量:<b>21</b></div>
                      <div>收藏量:<b>21</b></div>

                    </div>
                  </div>

                </div>
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <a href="#" class="media-object">
                      <video src="../static2/media/video.mp4" width="320" height="240" controls="controls">
                        Your browser does not support the video tag.
                      </video>
                    </a>
                  </div>
                  <div class="media-body">
                    <a class="media-heading title_news" href="../pages/detail.jsp" target="view_window">热点新闻</a>
                    <div class="media_content">
                      这里是新闻内这里是新这里是新闻内容你发附件闻内容你发附新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里件容这里是新闻内容你发附件你发附件
                    </div>
                    <div class="media_option">
                      <div>阅读量:<b>21</b></div>
                      <div>点赞量:<b>21</b></div>
                      <div>收藏量:<b>21</b></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="../static2/images/tx.jpg" alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <a class="media-heading title_news" href="../pages/detail.jsp" target="view_window">热点新闻</a>
                    <div class="media_content">
                      这里是新闻内这里是新这里是新闻内容你发附件闻内容你发附新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里件容这里是新闻内容你发附件你发附件
                    </div>
                    <div class="media_option">
                      <div>阅读量:<b>21</b></div>
                      <div>点赞量:<b>21</b></div>
                      <div>收藏量:<b>21</b></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="../static2/images/tx.jpg" alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <a class="media-heading title_news" href="../pages/detail.jsp" target="view_window">热点新闻</a>
                    <div class="media_content">
                      这里是新闻内这里是新这里是新闻内容你发附件闻内容你发附新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里件容这里是新闻内容你发附件你发附件
                    </div>
                    <div class="media_option">
                      <div>阅读量:<b>21</b></div>
                      <div>点赞量:<b>21</b></div>
                      <div>收藏量:<b>21</b></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="../static2/images/tx.jpg" alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <a class="media-heading title_news" href="../pages/detail.jsp" target="view_window">热点新闻</a>
                    <div class="media_content">
                      这里是新闻内这里是新这里是新闻内容你发附件闻内容你发附新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里新闻内容你发附件闻内容你发附件容这里件容这里是新闻内容你发附件你发附件
                    </div>
                    <div class="media_option">
                      <div>阅读量:<b>21</b></div>
                      <div>点赞量:<b>21</b></div>
                      <div>收藏量:<b>21</b></div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <!--  页角 -->
            <div class="footer">
            </div>
          </div>
          <!--  页脚 -->

        </div>
        <!-- 内容 -->

      </div>
    </div>
  </div>


  <script type="text/javascript" src="../static2/js/basic.js"></script>
</body>

</html>