package webAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import tools.hbnHelper;

public class exitAction extends ActionSupport{
	public String exit()throws Exception{
		ActionContext context=ActionContext.getContext();
		context.getSession().remove("username");
		context.getSession().remove("email");
		context.getSession().remove("password");
		return SUCCESS;
	}
}
