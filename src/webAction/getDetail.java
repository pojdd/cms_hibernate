package webAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cms_hibernate.article;
import tools.hbnHelper;

public class getDetail extends ActionSupport{
	public int id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArticle()throws Exception{
		ActionContext context=ActionContext.getContext();
//		System.out.println(id);
		article article=hbnHelper.getArticleByid(id);
//		context.put("username", article.getUser().getNickname());
//		context.put("content", article.getUser().getNickname());
//		context.getSession().put("arcitle", hbnHelper.getArticleByid(id));
//		System.out.println(article.getSummary());
		context.put("content", article.getContent());
		context.put("title", article.getTitle());
		context.put("date", article.getReleaseDate());
		context.put("nickname", article.getUser().getNickname());
		context.put("likes",article.getUser_likes().size());
		context.put("cloo",article.getUser_collection().size());
		context.put("articleid", id);
		return SUCCESS;
	}
}
