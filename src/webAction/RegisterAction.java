package webAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import tools.hbnHelper;
import tools.message;

public class RegisterAction extends ActionSupport{
	private String email;
	private String name;
	private String password;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String register()throws Exception{
		ActionContext context=ActionContext.getContext();
		message message=new message();
		if(email.length()<4) {
			context.put("error", "邮箱太短了");
			return ERROR;
		}else if(password.length()<4) {
			context.put("error", "密码太短了");
			return ERROR;
		}
		message=hbnHelper.register(name,email, password);
		System.out.println(message.type);
		if(message.type==1){
			context.getSession().put("username", name);
			context.put("username", name);
			return SUCCESS;
		}else {
			context.put("error", message.context);
			return ERROR;
		}
	}
	
}
