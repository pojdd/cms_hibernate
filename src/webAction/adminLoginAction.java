package webAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Date;
import java.util.Timer;

import javax.servlet.http.Cookie;

import tools.MD5;
import tools.hbnHelper;

public class adminLoginAction extends ActionSupport {
	private String email;
	private String password;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String login()throws Exception{
		ActionContext context=ActionContext.getContext();
		System.out.println(email+password);
		if(hbnHelper.AdminLogin(email, password)){
			context.getSession().put("username", hbnHelper.getNickName(email));
			context.getSession().put("email", email);
			context.getSession().put("password", hbnHelper.getSsPassword(password));
//			context.getSession()
			return SUCCESS;
		}else {
			context.put("error", "用户名或密码错误");
			return ERROR;
		}
	}
}
