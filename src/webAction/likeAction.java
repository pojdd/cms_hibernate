package webAction;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cms_hibernate.article;
import tools.hbnHelper;

public class likeAction extends ActionSupport {
	private int articleid;
	
	public int getArticleid() {
		return articleid;
	}

	public void setArticleid(int articleid) {
		this.articleid = articleid;
	}

	public String addnum()throws Exception{
		ActionContext context=ActionContext.getContext();
		String msg=hbnHelper.like((String)context.getSession().get("email"),articleid);
		
		article article=hbnHelper.getArticleByid(articleid);

		context.put("content", article.getContent());
		context.put("title", article.getTitle());
		context.put("date", article.getReleaseDate());
		context.put("nickname", article.getUser().getNickname());
		context.put("likes",article.getUser_likes().size());
		context.put("cloo",article.getUser_collection().size());
		context.put("articleid", articleid);
		context.put("msg", msg);
		return SUCCESS;
	}
}
