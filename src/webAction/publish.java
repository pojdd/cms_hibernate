package webAction;

import java.util.List;

import javax.servlet.http.Cookie;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cms_hibernate.article;
import tools.hbnHelper;

public class publish extends ActionSupport {
	public String getPages()throws Exception{
		ActionContext context=ActionContext.getContext();
		String email=(String) context.getSession().get("email");
		if(email==null)return ERROR;
		List<article> list= hbnHelper.getAllArticleByUserAccount(email);
		if(list==null)return SUCCESS;;
//		System.out.println(list.get(0).getUser_likes().size());
		context.put("list",list);
		context.put("listsize", list.size());
		return SUCCESS;
	}
}
