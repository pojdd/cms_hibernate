package webAction;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import tools.MD5;
import tools.hbnHelper;

public class UploadAction extends ActionSupport {

    private File uploadImage; //得到上传的文件
    private String uploadImageContentType; //得到文件的类型
    private String uploadImageFileName; //得到文件的名称
    
    private File uploadMovie; //得到上传的文件
    private String uploadMovieContentType; //得到文件的类型
    private String uploadMovieFileName; //得到文件的名称
    
    private File uploadMusic; //得到上传的文件
    private String uploadMusicContentType; //得到文件的类型
    private String uploadMusicFileName; //得到文件的名称
    
    private String title;
    private String content;
    
    
    static String Url="http://127.0.0.1:81/res/";//完整url路径
    
    public String execute(){
    	ActionContext context=ActionContext.getContext();
        System.out.println("fileName:"+this.getUploadImageFileName());
        System.out.println("contentType:"+this.getUploadImageContentType());
        System.out.println("File:"+this.getUploadImage());
        
        //获取要保存文件夹的物理路径(绝对路径)
        String realPath=ServletActionContext.getServletContext().getRealPath("/upload");
        File file = new File("C:\\phpStudy\\PHPTutorial\\WWW\\res");
        
        //测试此抽象路径名表示的文件或目录是否存在。若不存在，创建此抽象路径名指定的目录，包括所有必需但不存在的父目录。
        if(!file.exists())file.mkdirs();
        
        try {
            //保存文件
        	String filemd51=MD5.getFileMD5(uploadImage)+".png";
            FileUtils.copyFile(uploadImage, new File(file,filemd51));
            
            String filemd52=MD5.getFileMD5(uploadMovie)+".avi";
            FileUtils.copyFile(uploadMovie, new File(file,filemd52));
            
            String filemd53=MD5.getFileMD5(uploadMusic)+".mp3";
            FileUtils.copyFile(uploadMusic, new File(file,filemd53));
            
            String email=(String) context.getSession().get("email");
            hbnHelper.addArticle(email, title, content, Url+filemd53, Url+filemd51, Url+filemd52);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public File getUploadImage() {
        return uploadImage;
    }

    public void setUploadImage(File uploadImage) {
        this.uploadImage = uploadImage;
    }

    public String getUploadImageContentType() {
        return uploadImageContentType;
    }

    public void setUploadImageContentType(String uploadImageContentType) {
        this.uploadImageContentType = uploadImageContentType;
    }

    public String getUploadImageFileName() {
        return uploadImageFileName;
    }

    public void setUploadImageFileName(String uploadImageFileName) {
        this.uploadImageFileName = uploadImageFileName;
    }

	public File getUploadMovie() {
		return uploadMovie;
	}

	public void setUploadMovie(File uploadMovie) {
		this.uploadMovie = uploadMovie;
	}

	public String getUploadMovieContentType() {
		return uploadMovieContentType;
	}

	public void setUploadMovieContentType(String uploadMovieContentType) {
		this.uploadMovieContentType = uploadMovieContentType;
	}

	public String getUploadMovieFileName() {
		return uploadMovieFileName;
	}

	public void setUploadMovieFileName(String uploadMovieFileName) {
		this.uploadMovieFileName = uploadMovieFileName;
	}

	public File getUploadMusic() {
		return uploadMusic;
	}

	public void setUploadMusic(File uploadMusic) {
		this.uploadMusic = uploadMusic;
	}

	public String getUploadMusicContentType() {
		return uploadMusicContentType;
	}

	public void setUploadMusicContentType(String uploadMusicContentType) {
		this.uploadMusicContentType = uploadMusicContentType;
	}

	public String getUploadMusicFileName() {
		return uploadMusicFileName;
	}

	public void setUploadMusicFileName(String uploadMusicFileName) {
		this.uploadMusicFileName = uploadMusicFileName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
    
    
}