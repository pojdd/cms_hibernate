package webAction;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cms_hibernate.article;
import tools.hbnHelper;

public class getHomePages extends ActionSupport {
	public String getPages()throws Exception{
		ActionContext context=ActionContext.getContext();
		int num=3;
		List<article> list= hbnHelper.getGoodAC(num);
		context.put("list",list);
		context.put("listsize", list.size());
		return SUCCESS;
	}
}
