package cms_hibernate;

import java.util.Date;

public class user_report {
	private Integer id;
	private Date reportDate;
	private String reportType;
	private String reportContent;
	private Integer state;
	private Date processDate;
	private String processContent;
	
	private user user;
	private article acticle;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getReportContent() {
		return reportContent;
	}
	public void setReportContent(String reportContent) {
		this.reportContent = reportContent;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	public String getProcessContent() {
		return processContent;
	}
	public void setProcessContent(String processContent) {
		this.processContent = processContent;
	}
	public user getUser() {
		return user;
	}
	public void setUser(user user) {
		this.user = user;
	}
	public article getActicle() {
		return acticle;
	}
	public void setActicle(article acticle) {
		this.acticle = acticle;
	}
	@Override
	public String toString() {
		return "user_report [id=" + id + ", reportDate=" + reportDate + ", reportType=" + reportType
				+ ", reportContent=" + reportContent + ", state=" + state + ", processDate=" + processDate
				+ ", processContent=" + processContent + ", user=" + user + ", acticle=" + acticle + "]";
	}
	
}

