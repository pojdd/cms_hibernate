package cms_hibernate;

import java.util.HashSet;
import java.util.Set;

public class user {
	private Integer id;
	private String nickname;
	private String account;
	private String password;
	private String role;
	private Set<article>article=new HashSet<article>();
	private Set<user_likes>user_likes=new HashSet<user_likes>();
	private Set<user_collection>user_collection=new HashSet<user_collection>();
	private Set<user_report>user_report=new HashSet<user_report>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Set<article> getArticle() {
		return article;
	}
	public void setArticle(Set<article> article) {
		this.article = article;
	}
	public Set<user_likes> getUser_likes() {
		return user_likes;
	}
	public void setUser_likes(Set<user_likes> user_likes) {
		this.user_likes = user_likes;
	}
	public Set<user_collection> getUser_collection() {
		return user_collection;
	}
	public void setUser_collection(Set<user_collection> user_collection) {
		this.user_collection = user_collection;
	}
	public Set<user_report> getUser_report() {
		return user_report;
	}
	public void setUser_report(Set<user_report> user_report) {
		this.user_report = user_report;
	}
	@Override
	public String toString() {
		return "user [id=" + id + ", nickname=" + nickname + ", account=" + account + ", password=" + password
				+ ", role=" + role + ", article=" + article + ", user_likes=" + user_likes + ", user_collection="
				+ user_collection + ", user_report=" + user_report + "]";
	}
	
}
