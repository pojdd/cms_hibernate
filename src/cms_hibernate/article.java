package cms_hibernate;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class article {
	private Integer id;
	private String title;
	private String summary;
	private String content;
	private Date releaseDate;
	private Integer clickTimes;
	private String backgroundMusicUrl;
	private String pictureUrls;
	private String videoUrl;
	private Integer type;
	private Integer state;
	private user user;
	private category category;
	private Set<user_likes>user_likes=new HashSet<user_likes>();
	private Set<user_collection>user_collection=new HashSet<user_collection>();
	private Set<user_report>user_report=new HashSet<user_report>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Integer getClickTimes() {
		return clickTimes;
	}
	public void setClickTimes(Integer clickTimes) {
		this.clickTimes = clickTimes;
	}
	public String getBackgroundMusicUrl() {
		return backgroundMusicUrl;
	}
	public void setBackgroundMusicUrl(String backgroundMusicUrl) {
		this.backgroundMusicUrl = backgroundMusicUrl;
	}
	public String getPictureUrls() {
		return pictureUrls;
	}
	public void setPictureUrls(String pictureUrls) {
		this.pictureUrls = pictureUrls;
	}
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public user getUser() {
		return user;
	}
	public void setUser(user user) {
		this.user = user;
	}
	public category getCategory() {
		return category;
	}
	public void setCategory(category category) {
		this.category = category;
	}
	public Set<user_likes> getUser_likes() {
		return user_likes;
	}
	public void setUser_likes(Set<user_likes> user_likes) {
		this.user_likes = user_likes;
	}
	public Set<user_collection> getUser_collection() {
		return user_collection;
	}
	public void setUser_collection(Set<user_collection> user_collection) {
		this.user_collection = user_collection;
	}
	public Set<user_report> getUser_report() {
		return user_report;
	}
	public void setUser_report(Set<user_report> user_report) {
		this.user_report = user_report;
	}
	@Override
	public String toString() {
		return "article [id=" + id + ", title=" + title + ", summary=" + summary + ", content=" + content
				+ ", releaseDate=" + releaseDate + ", clickTimes=" + clickTimes + ", backgroundMusicUrl="
				+ backgroundMusicUrl + ", pictureUrls=" + pictureUrls + ", videoUrl=" + videoUrl + ", type=" + type
				+ ", state=" + state + ", user=" + user + ", category=" + category + ", user_likes=" + user_likes
				+ ", user_collection=" + user_collection + ", user_report=" + user_report + "]";
	}
	
}
