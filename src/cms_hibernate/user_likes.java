package cms_hibernate;

import java.util.Date;

public class user_likes {
	private Integer id;
	private Date date;
	private Integer state;
	private user user;
	private article acticle;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public user getUser() {
		return user;
	}
	public void setUser(user user) {
		this.user = user;
	}
	public article getActicle() {
		return acticle;
	}
	public void setActicle(article acticle) {
		this.acticle = acticle;
	}
	@Override
	public String toString() {
		return "user_likes [id=" + id + ", date=" + date + ", state=" + state + ", user=" + user + ", acticle="
				+ acticle + "]";
	}
	
	
}
