package cms_hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import com.sun.org.apache.xerces.internal.util.URI;

public class articleTest {

	@Test
	public void test() {
		Configuration config=new Configuration().configure("/cms_hibernate/hibernate.cfg.xml");
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction t=session.beginTransaction();
		
		user a=new user();
		article b=new article();
		category c=new category();
		category d=new category();
		
		a.getArticle().add(b);
		c.getArticle().add(b);
		c.getChilds().add(d);//发布文章
		
		for(int t1=0;t1<10;t1++) {//创建机器人素质三连
			user rboot=new user();
			user_collection user1=new user_collection();
			user_likes user2=new user_likes();
			user_report user_report=new user_report();
			
			rboot.setNickname("机器人"+t1);
			rboot.getUser_likes().add(user2);//点赞
			rboot.getUser_collection().add(user1);//收藏
			rboot.getUser_report().add(user_report);//举报
			
			b.getUser_likes().add(user2);//点赞
			b.getUser_collection().add(user1);//收藏
			b.getUser_report().add(user_report);//举报
			
			session.save(rboot);
			session.save(user1);
			session.save(user2);
			session.save(user_report);
		}
		
		
		
		session.save(a);
		session.save(b);
		session.save(c);
		session.save(d);
		
		t.commit();
		session.close();
		sessionFactory.close();
	}

}
