package cms_hibernate;

import java.util.HashSet;
import java.util.Set;

public class category {
	private Integer id;
	private String name;
	private String description;
	private category parent;
	private Set<category>childs=new HashSet<category>();
	private Set<article>article=new HashSet<article>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public category getParent() {
		return parent;
	}
	public void setParent(category parent) {
		this.parent = parent;
	}
	public Set<category> getChilds() {
		return childs;
	}
	public void setChilds(Set<category> childs) {
		this.childs = childs;
	}
	public Set<article> getArticle() {
		return article;
	}
	public void setArticle(Set<article> article) {
		this.article = article;
	}
	@Override
	public String toString() {
		return "category [id=" + id + ", name=" + name + ", description=" + description + ", parent=" + parent
				+ ", childs=" + childs + ", article=" + article + "]";
	}
	
	
}
