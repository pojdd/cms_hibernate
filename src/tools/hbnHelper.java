package tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import cms_hibernate.article;
import cms_hibernate.user;
import cms_hibernate.user_collection;
import cms_hibernate.user_likes;

public class hbnHelper {
	public static Boolean login(String email,String password) {
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
//		Transaction t=session.beginTransaction();
		//HQL 查询
		
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		criteria.add(Restrictions.eq("password",password));
		
		List<user>cs=criteria.list();
		if(cs.size()>0)return true;
		session.close();
		sessionFactory.close();
		return false;
	}
	public static Boolean AdminLogin(String email,String password) {
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
//		Transaction t=session.beginTransaction();
		//HQL 查询
		
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		criteria.add(Restrictions.eq("password",password));
		criteria.add(Restrictions.eq("role","0"));//验证role是0表示是管理员
		
		List<user>cs=criteria.list();
		if(cs.size()>0)return true;
		session.close();
		sessionFactory.close();
		return false;
	}
	public static Boolean TestAccount(String email) {
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
//		Transaction t=session.beginTransaction();
		//HQL 查询
		
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		
		List<user>cs=criteria.list();
		if(cs.size()==0)return true;
		session.close();
		sessionFactory.close();
		return false;
	}
	public static message register(String name,String email,String password) {
		message msg=new message();
		if(name==null||name.length()<4) {
			msg.type=0;
			msg.context="名称不合法";
		}
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction t=session.beginTransaction();
		
		
		user user=new user();
		user.setAccount(email);
		user.setPassword(password);
		user.setNickname(name);
		user.setRole("1");//设置为普通用户
		
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		if(cs.size()==0) {
			msg.type=1;
			session.save(user);
		}
		else {
			msg.type=0;
			msg.context="邮箱被注册过了";
		}
		
		t.commit();
		session.close();
		sessionFactory.close();
		return msg;
	}
	public static String getNickName(String email) {
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		user user=new user();
		user.setAccount(email);
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		String name=null;
		if(cs.size()!=0)name=cs.get(0).getNickname();
		session.close();
		sessionFactory.close();
		return name;
		
	}

	public static String getSsPassword(String password) {//每天半夜12点更新
		Date date=new Date();
		return MD5.getMD5(password+date.getDay());
	}
	public static boolean CheckSsPassword(String email,String password) {
		if(email==null||password==null)return false;
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		user user=new user();
//		System.out.println(email+password);
//		user.setPassword(password);
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		String passwords=null;
		if(cs.size()>0)passwords=cs.get(0).getPassword();
//		System.out.println(passwords);
		passwords=getSsPassword(passwords);
		session.close();
		sessionFactory.close();
		if(passwords==null)return false;
		if(password.equals(passwords))return true;
		else return false;
	}
	public static List<article> requreArticle(){
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		Criteria criteria=session.createCriteria(article.class);
		List<article>list=criteria.list();
		for(article temp:list) {
			if(temp.getSummary()==null)continue;
			if(temp.getSummary().length()>50)
			temp.setSummary(temp.getSummary().substring(0, 49));
		}
		
		session.close();
		sessionFactory.close();
		return list;
	}
	public static boolean CheckAdminSsPassword(String email, String password) {
		if(email==null||password==null)return false;
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		user user=new user();
//		System.out.println(email+password);
//		user.setPassword(password);
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		String passwords=null;
		if(cs.size()>0)
		{
			passwords=cs.get(0).getPassword();
			if(cs.get(0).getRole()=="1")return false;
		}
//		System.out.println(passwords);
		passwords=getSsPassword(passwords);
		session.close();
		sessionFactory.close();
		if(passwords==null)return false;
		if(password.equals(passwords))return true;
		else return false;
	}
	public static article getArticleByid(int id) {
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		article article=null;
		Criteria criteria=session.createCriteria(article.class);
		criteria.add(Restrictions.eq("id",id));
		List<article>cs=criteria.list();
		if(cs.size()!=0)article=cs.get(0);
		article.getUser().getNickname();
		article.getUser_likes().size();
		article.getUser_collection().size();
		session.close();
		sessionFactory.close();
		return article;
	}
	public static String like(String email, int articleid) {
		// TODO Auto-generated method stub
		
		if(email==null)return "未登录";
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction t=session.beginTransaction();
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		if(cs.size()==0)return "账号过期";
		user user=cs.get(0);//点赞用户
		
		
		Criteria criteria1=session.createCriteria(article.class);
		criteria1.add(Restrictions.eq("id",articleid));
		List<article>ar=criteria1.list();
		if(ar.size()==0)return "文章异常";
		article ax=ar.get(0);//点赞文章
		
		int id=user.getId();
//		for(user_likes ss:user.getUser_likes()) {//使用HRL查询效率更高
//			if(ss.getUser().getId()==id)return false;
//		}
		Query q=session.createQuery("from user_likes uc where uc.user.id="+id+"and uc.acticle.id="+articleid);
//		System.out.println(q.list().size());
		if(q.list().size()>0) {
			session.delete(q.list().get(0));
			t.commit();
			session.close();
			sessionFactory.close();
			return "点赞取消";//点赞过了
		}
		
		user_likes like=new user_likes();
		Date date=new Date();
		like.setDate(date);
		like.setState(0);
		user.getUser_likes().add(like);
		ax.getUser_likes().add(like);
		
		session.save(like);
		session.save(user);
		session.save(ax);
		
		t.commit();
		session.close();
		sessionFactory.close();
		return "点赞成功";
	}
	public static String cloo(String email, int articleid) {
		// TODO Auto-generated method stub
		if(email==null)return "未登录";
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction t=session.beginTransaction();
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		if(cs.size()==0)return "账号过期";
		user user=cs.get(0);//收藏用户
		
		
		Criteria criteria1=session.createCriteria(article.class);
		criteria1.add(Restrictions.eq("id",articleid));
		List<article>ar=criteria1.list();
		if(ar.size()==0)return "账号过期";
		article ax=ar.get(0);//收藏文章
		
		int id=user.getId();
		Query q=session.createQuery("from user_collection uc where uc.user.id="+id+"and uc.acticle.id="+articleid);
//		System.out.println(q.list().size());
		if(q.list().size()>0) {
			session.delete(q.list().get(0));
			t.commit();
			session.close();
			sessionFactory.close();
			return "收藏取消";//收藏过了可以取消收藏
		}
		
		user_collection like=new user_collection();
		Date date=new Date();
		like.setDate(date);
		user.getUser_collection().add(like);
		ax.getUser_collection().add(like);
		
		session.save(like);
		session.save(user);
		session.save(ax);
		
		t.commit();
		session.close();
		sessionFactory.close();
		return "收藏成功";
	}
	public static List<article> getGoodAC(int num) {//推荐num篇文章
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		Criteria criteria=session.createCriteria(article.class);
		criteria.add(Restrictions.eq("state",1));
		criteria.addOrder(Order.desc("clickTimes"));
		criteria.setMaxResults(num);//设置返回记录数
		List<article>list=criteria.list();//获取推荐集
//		list.get(0).getUser_likes().size();
//		System.out.println(list.size());
		for(article temp:list) {
//			System.out.println(temp.getClickTimes());//验证降序排序
			temp.getUser_likes().size();
			temp.getUser_collection().size();
		}
//		System.out.println("dd");
//		if(list.size()>num) {//裁减推荐集
//			list=list.subList(0, num);
//		}
//		System.out.println(list.size());
		session.close();
		sessionFactory.close();
		return list;
	}
	public static List<article> getAllArticleByUserAccount(String email) {
		// TODO Auto-generated method stub
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>list=criteria.list();
		
		for(article temp:list.get(0).getArticle()) {
			temp.getUser_likes().size();//调用方法，让sesson获取数据
			temp.getUser_collection().size();
		}
		
		if(list.size()==0)return null;
		Set<article> articles = list.get(0).getArticle();
		List <article> ac=new ArrayList<>();
		if(articles.size()>0)
		{
			ac.addAll(articles);
		}
		session.close();
		sessionFactory.close();
		return ac;
	}
	public static void addArticle(String email, String title,String content,
			String backgroundMusicUrl, String pictureUrls, String videoUrl) {
		Configuration config=new Configuration().configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction t=session.beginTransaction();
		
		Criteria criteria=session.createCriteria(user.class);
		criteria.add(Restrictions.eq("account",email));
		List<user>cs=criteria.list();
		if(cs.size()==0) {
			t.commit();
			session.close();
			sessionFactory.close();
			return;
		}
		user user=cs.get(0);
		
		Date releaseDate=new Date();
		article a=new article();
		a.setUser(user);
		a.setTitle(title);
		a.setContent(content);
		a.setReleaseDate(releaseDate);
		a.setBackgroundMusicUrl(backgroundMusicUrl);
		a.setPictureUrls(pictureUrls);
		a.setVideoUrl(videoUrl);
		a.setClickTimes(0);
		//user.getArticle().add(a);
		
		
		session.save(a);
		session.save(user);
		
		
		t.commit();
		session.close();
		sessionFactory.close();
	}

}
